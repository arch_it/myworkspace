package com.robot.test;

import java.util.Map;
import java.util.Set;

import junit.framework.JUnit4TestAdapter;
import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;

import com.robot.constants.ActionConstant;
import com.robot.service.model.Direction;
import com.robot.service.model.Position;
import com.robot.services.exception.RobotApplicationException;
import com.robot.webapi.service.RobotActionServiceImpl;

public class RobotActionTest extends TestCase {
	String NAME = "OPTIMUSPRIME"+Math.random();
	Position POS;
	RobotActionServiceImpl robotServiceImpl;
	Integer xAxis = 0;
	Integer yAxis = 0;
	Direction face = Direction.NORTH;
	Position expected ;

	@Before
	public void setUp() {

		POS = new Position(xAxis, yAxis, face);
		try {
			robotServiceImpl = new RobotActionServiceImpl();
			robotServiceImpl.createRobot(NAME);
			POS = robotServiceImpl.placeRobot(NAME, POS);
			System.out.println("Created Robot-" +NAME);
			System.out.println("Placed Robot-" +POS.toString());
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	@Test 
		public void testMoveEast()  {
		

		try {
			expected = new Position(5, 0, Direction.EAST);
			robotServiceImpl.moveRobot(NAME, ActionConstant.TURNLEFT);
			//robotServiceImpl.getPosition(NAME);
			robotServiceImpl.moveRobot(NAME, ActionConstant.STEPFORWARD);
			//robotServiceImpl.getPosition(NAME);
			robotServiceImpl.moveRobot(NAME, ActionConstant.STEPFORWARD);
			//robotServiceImpl.getPosition(NAME);
			robotServiceImpl.moveRobot(NAME, ActionConstant.STEPFORWARD);
			//robotServiceImpl.getPosition(NAME);
			robotServiceImpl.moveRobot(NAME, ActionConstant.STEPFORWARD);
			//robotServiceImpl.getPosition(NAME);
			robotServiceImpl.moveRobot(NAME, ActionConstant.STEPFORWARD);
			//robotServiceImpl.getPosition(NAME);
			Map<String, Position> map = robotServiceImpl.getAllPositions(NAME);
			Set<String> keys = map.keySet();
			for(String key : keys )
			{
				System.out.println("\n" + key+ " "+map.get(key));
			}
				
			
		//	System.out.println(expected+"\n"+robotServiceImpl.getPosition(NAME));
			assertTrue(expected.equals(robotServiceImpl.getPosition(NAME)));

		} catch (Exception e) {
			System.out.println(e.getMessage());
			Map<String, Position> map = null;
			try {
				map = robotServiceImpl.getAllPositions(NAME);
			} catch (RobotApplicationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Set<String> keys = map.keySet();
			for(String key : keys )
			{
				System.out.println("\n" + key+ " "+map.get(key));
			}
		}

	}
	
@Test 
	public void testMoveSouth() {
		
		try {
			expected = new Position(0, 0, Direction.SOUTH);
			robotServiceImpl.moveRobot(NAME, ActionConstant.TURNLEFT);
			robotServiceImpl.getPosition(NAME);
			robotServiceImpl.moveRobot(NAME, ActionConstant.TURNLEFT);
			robotServiceImpl.getPosition(NAME);
			robotServiceImpl.moveRobot(NAME, ActionConstant.STEPFORWARD);
			robotServiceImpl.getPosition(NAME);
			robotServiceImpl.moveRobot(NAME, ActionConstant.STEPFORWARD);
			robotServiceImpl.getPosition(NAME);
			robotServiceImpl.moveRobot(NAME, ActionConstant.STEPFORWARD);
			robotServiceImpl.getPosition(NAME);
			robotServiceImpl.moveRobot(NAME, ActionConstant.STEPFORWARD);
			robotServiceImpl.getPosition(NAME);
			assertTrue(expected.equals(robotServiceImpl.getPosition(NAME)));

		} catch (Exception e) {
			System.out.println(e.getMessage());
			Map<String, Position> map = null;
			try {
				map = robotServiceImpl.getAllPositions(NAME);
			} catch (RobotApplicationException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Set<String> keys = map.keySet();
			for(String key : keys )
			{
				System.out.println("\n" + key+ " "+map.get(key));
			}
		}
	}
}
