/**
 * 
 */
package com.robot.constants;

/**
 * @author ARCHANA RAJASEKARAN
 * @Purpose only has restricted set of actions the toy robot can perform
 */
public enum ActionConstant {
	TURNRIGHT,TURNLEFT,STEPFORWARD,PLACE

}
