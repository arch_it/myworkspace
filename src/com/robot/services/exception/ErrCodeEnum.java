/**
 * 
 */
package com.robot.services.exception;

/**
 * @author ARCHANA RAJASEKARAN
 *
 */
public enum ErrCodeEnum {
	ERR_ROBOT_NOT_CREATED, ERR_OUT_OF_BOUNDARY,ERR_ROBOT_ALREADY_AVAILABLE,ERR_ACTION_FAILED

}
