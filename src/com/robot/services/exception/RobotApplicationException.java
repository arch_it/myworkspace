package com.robot.services.exception;

import com.robot.constants.RobotConstants;

public class RobotApplicationException extends Exception {
	ErrCodeEnum ERR_CODE;

	
	private static final long serialVersionUID = 1L;

	public RobotApplicationException(ErrCodeEnum codeEnum) {
		this.ERR_CODE = codeEnum;
	}

	@Override
	public String getMessage() {

		if (ERR_CODE.equals(ErrCodeEnum.ERR_ROBOT_NOT_CREATED)) {
			return RobotConstants.ERR_ROBOT_NOT_AVAILABLE;
		} else if (ERR_CODE.equals(ErrCodeEnum.ERR_OUT_OF_BOUNDARY)) {
			return RobotConstants.ERR_OUT_OF_BOUNDARY;
		} else if (ERR_CODE.equals(ErrCodeEnum.ERR_ROBOT_ALREADY_AVAILABLE)) {
			return RobotConstants.ERR_ROBOT_ALREADY_AVAILABLE;
		} else if (ERR_CODE.equals(ErrCodeEnum.ERR_ACTION_FAILED)) {
			return RobotConstants.ERR_ACTION_FAILED;
		} else {
			return "No message Available";
		}

	}
}
