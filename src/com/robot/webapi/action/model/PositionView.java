package com.robot.webapi.action.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PositionView {
	String angle;
	int x_pos;
	public String getAngle() {
		return angle;
	}
	public void setAngle(String angle) {
		this.angle = angle;
	}
	public int getX_pos() {
		return x_pos;
	}
	public void setX_pos(int x_pos) {
		this.x_pos = x_pos;
	}
	public int getY_pos() {
		return y_pos;
	}
	public void setY_pos(int y_pos) {
		this.y_pos = y_pos;
	}
	int y_pos;
  
}
