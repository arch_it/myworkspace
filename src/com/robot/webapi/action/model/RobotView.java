package com.robot.webapi.action.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class RobotView {

	String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
