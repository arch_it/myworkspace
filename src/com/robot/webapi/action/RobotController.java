package com.robot.webapi.action;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.robot.constants.ActionConstant;
import com.robot.service.model.Direction;
import com.robot.service.model.Position;
import com.robot.services.exception.RobotApplicationException;
import com.robot.webapi.action.model.PositionView;
import com.robot.webapi.action.model.RobotView;
import com.robot.webapi.service.RobotActionServiceImpl;
import com.robot.webapi.service.RobotActionServiceInterface;



@Path("/robot")
public class RobotController {
	RobotActionServiceInterface roboticService = new RobotActionServiceImpl();
	  // This method is called if TEXT_PLAIN is request
	  @POST
	  @Path("/{name}")
	 @Produces(MediaType.TEXT_PLAIN)
	  public Response createRobot(@PathParam("name") String name) {
		
		  try {
			roboticService.createRobot(name);
			
		} catch (RobotApplicationException e) {
			//return RobotConstants.ERR_ROBOT_ALREADY_AVAILABLE + "NAME:" +name;
			return Response.status(303).build();
		}
		  return Response.ok().build();
		  
	    //return name + " Created Successfully!";
	  }
	  /*@GET
	  @Path("/{name}/d/{news}/{xaxis}/{yaxis}")
	 @Produces(MediaType.TEXT_PLAIN)
	  public String placeRobot(@PathParam("name") String name,@PathParam("news") String direction,@PathParam("xaxis") String x,@PathParam("yaxis") String y ) {
		  
		  try {
			  Position pos;
			  switch((direction).toUpperCase()){
			  
			  case  "NORTH" :
				   pos = new Position(new Integer(x), new Integer(y),Direction.NORTH )  ;
				break;  
			  case  "SOUTH" :
				   pos = new Position(new Integer(x), new Integer(y),Direction.SOUTH )  ;
					break;  
			  case  "EAST" :
				   pos = new Position(new Integer(x), new Integer(y),Direction.EAST )  ;
					break;  
			  case  "WEST" :
				   pos = new Position(new Integer(x), new Integer(y),Direction.WEST )  ;
					break;
				default :
					return "could not place robot as the direction param is not correct";
			  }
			return   (roboticService.placeRobot(name, pos)).toString();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			return "supplied x and y axis values are incorrect";
		} catch (RobotApplicationException e) {
			// TODO Auto-generated catch block
			return e.getMessage();
		}
	  } */
		 @POST
		 @Path("/{name}/position/")
		 @Consumes(MediaType.APPLICATION_JSON)
		 @Produces({ MediaType.APPLICATION_JSON })
		  public Response placeRobot(PositionView posview,@PathParam("name") String name) {
			  
			  try {
				  System.out.println("POSITION:::::"+posview.getX_pos());
				  System.out.println("name:::::"+name);
				  Position pos;
				  switch((posview.getAngle()).toUpperCase()){
				  
				  case  "NORTH" :
					   pos = new Position(new Integer(posview.getX_pos()), new Integer(posview.getY_pos()),Direction.NORTH )  ;
					break;  
				  case  "SOUTH" :
					   pos = new Position(new Integer(posview.getX_pos()), new Integer(posview.getY_pos()),Direction.SOUTH )  ;
						break;  
				  case  "EAST" :
					   pos = new Position(new Integer(posview.getX_pos()), new Integer(posview.getY_pos()),Direction.EAST )  ;
						break;  
				  case  "WEST" :
					   pos = new Position(new Integer(posview.getX_pos()), new Integer(posview.getY_pos()),Direction.WEST )  ;
						break;
					default :
						return Response.status(400).build();

				  }
				  
				  Position currentPosition = roboticService.placeRobot(name, pos);
				  PositionView positionView = new PositionView();
				  positionView.setAngle(currentPosition.getFace().name());
				  positionView.setX_pos(currentPosition.getxAxis());
				  positionView.setY_pos(currentPosition.getyAxis());
				  return Response.ok(positionView).build();
				 
				
			} catch (NumberFormatException e) {
				return Response.status(400).build();
			} catch (RobotApplicationException e) {
				return Response.status(400).build();
			}
			  
		  
	  }
	  
	  
	  @PUT
	  @Path("/{name}/position/{pos}")
	  @Produces({ MediaType.APPLICATION_JSON })
	  public Response moveRobot(@PathParam("name") String name, @PathParam("pos") String pos) {
		  try {
			  if(pos.equalsIgnoreCase("left"))
				  roboticService.moveRobot(name,ActionConstant.TURNLEFT);
			  else if(pos.equalsIgnoreCase("right"))
					roboticService.moveRobot(name,ActionConstant.TURNRIGHT);
			  else if(pos.equalsIgnoreCase("move"))
					roboticService.moveRobot(name,ActionConstant.STEPFORWARD);
			  else  {
				  return Response.status(400).build();
				  }
			  Position currentPosition = roboticService.getPosition(name);
			  PositionView positionView = new PositionView();
			  positionView.setAngle(currentPosition.getFace().name());
			  positionView.setX_pos(currentPosition.getxAxis());
			  positionView.setY_pos(currentPosition.getyAxis());
			  return Response.ok(positionView).build();
			  
				  
				 
		}  catch (RobotApplicationException e) {
			return Response.status(204).build();
		} catch (Exception e) {
			return Response.status(400).build();
		}

	  }
	  @GET
	  @Path("/{name}/position/")
	  @Produces({ MediaType.APPLICATION_JSON })
	  public Response getPosition(@PathParam("name") String name) {
		  try {
			  Position pos = roboticService.getPosition(name);
			  PositionView positionView = new PositionView();
			  positionView.setAngle(pos.getFace().name());
			  positionView.setX_pos(pos.getxAxis());
			  positionView.setY_pos(pos.getyAxis());
			  return Response.ok(positionView).build();
		  } catch (Exception e) {
			  return Response.status(204).build();
		}

	  }
	
	  @GET
	  @Path("/list")
	  @Produces({ MediaType.APPLICATION_JSON })
	  public Response getRobotList() {
		  try {
			  List<String> nameList = roboticService.getRobotList();
			  List<RobotView> viewList = new ArrayList();
			  for(String name: nameList){
				  RobotView rv = new RobotView();
				  rv.setName(name);
				  viewList.add(rv);
			  }
			  

			 return Response.ok(viewList).build();  
		  } catch (Exception e) {
			return Response.status(200).build();
		}

	  }

}
