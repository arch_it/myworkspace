package com.robot.webapi.service;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import com.robot.constants.ActionConstant;
import com.robot.service.RobotActionImpl;
import com.robot.service.RobotActionInterface;
import com.robot.service.model.Direction;
import com.robot.service.model.Position;
import com.robot.service.model.Robot;
import com.robot.services.exception.ErrCodeEnum;
import com.robot.services.exception.RobotApplicationException;

public final class RobotActionServiceImpl implements RobotActionServiceInterface{

	
	
	public synchronized Position placeRobot(String name, Position position) throws RobotApplicationException{
		try {
			if (RobotTracker.isAvailable(name)) {
				Robot r = RobotTracker.getRobot(name);

				r.validatePosition(position);
				r.setPosition(position);
				r.setPositionTracker(ActionConstant.PLACE, position);
				RobotTracker.setName(name, r);

				return r.getPosition();
			} else {
				System.out.println("Not availabe");
				throw new RobotApplicationException(
						ErrCodeEnum.ERR_ROBOT_NOT_CREATED);
			}
		} catch (RobotApplicationException e) {
			System.out.println(e.getMessage());
			throw e;
		}

	}
	
	public synchronized void createRobot(String name) throws RobotApplicationException{
		Robot iRobot = new Robot(name, new Position(0,0,Direction.NORTH)); //setting ROBOT with default position
		if( RobotTracker.isAvailable(name))
			throw new RobotApplicationException(ErrCodeEnum.ERR_ROBOT_ALREADY_AVAILABLE);
		else
			RobotTracker.setName(name,iRobot);
		//iRobot.getPosition();
		
	}

	
	public synchronized Position moveRobot(String name,ActionConstant where) throws RobotApplicationException {
		Robot iRobot = RobotTracker.getRobot(name);
		if(null == iRobot) throw new RobotApplicationException(ErrCodeEnum.ERR_ROBOT_NOT_CREATED);
		RobotActionInterface actionImpl = new RobotActionImpl();
		//System.err.println(iRobot.getPosition().toString());
		if(where == ActionConstant.TURNRIGHT){
			iRobot.setPosition(actionImpl.turnRight(iRobot));
			//System.err.println(iRobot.getPosition().toString());
			iRobot.setPositionTracker(ActionConstant.TURNRIGHT, iRobot.getPosition());
		}else if (where == ActionConstant.TURNLEFT){
			iRobot.setPosition(actionImpl.turnLeft(iRobot));
			iRobot.setPositionTracker(ActionConstant.TURNLEFT, iRobot.getPosition());
		}else if (where == ActionConstant.STEPFORWARD) {
			iRobot.setPosition(actionImpl.move(iRobot));
			iRobot.setPositionTracker(ActionConstant.STEPFORWARD,
					iRobot.getPosition());
		}
		
		RobotTracker.setName(name, iRobot);
		//System.out.println("MAP POS_______________________"+RobotTracker.getRobot(name).getPosition().toString());
		return RobotTracker.getRobot(name).getPosition();
	}

	
	public synchronized Position getPosition(String name) throws RobotApplicationException {
		return RobotTracker.getRobot(name).getPosition();
		
	}
	public synchronized Map getAllPositions(String name) throws RobotApplicationException {
		return RobotTracker.getRobot(name).getPositionTracker();
		
	}
	public synchronized ArrayList<String> getRobotList() throws RobotApplicationException {
		
		Set<String> robotSet = RobotTracker.getRobotList();
		if(null != robotSet){
			return new ArrayList<String>(robotSet);
		}else {
			throw new RobotApplicationException(ErrCodeEnum.ERR_ACTION_FAILED);
		}
	
	}
	
	

}
