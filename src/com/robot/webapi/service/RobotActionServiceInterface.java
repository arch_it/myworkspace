/**
 * 
 */
package com.robot.webapi.service;

import java.util.ArrayList;

import com.robot.constants.ActionConstant;
import com.robot.service.model.Position;
import com.robot.services.exception.RobotApplicationException;

/**
 * @author ARCHANA RAJASEKARAN
 *
 */
public interface RobotActionServiceInterface {

	void createRobot(String name) throws RobotApplicationException;

	Position placeRobot(String name, Position position)
			throws RobotApplicationException;

	Position moveRobot(String name, ActionConstant direction)
			throws RobotApplicationException;

	Position getPosition(String name) throws RobotApplicationException;
	 ArrayList<String> getRobotList() throws RobotApplicationException;
}
