package com.robot.service;

import com.robot.constants.RobotConstants;
import com.robot.service.model.Direction;
import com.robot.service.model.Position;
import com.robot.service.model.Robot;
import com.robot.services.common.BaseRobotActions;
import com.robot.services.exception.ErrCodeEnum;
import com.robot.services.exception.RobotApplicationException;



/**
 * @author ARCHANA RAJASEKARAN
 * @Purpose This implementation of Robot Action would have actions supporting
 *           only a 2D plane movement of a robot with elementary movements
 *           like turnLeft and right for 90Degrees and moveforward. 
 *           {@Robot} should have basic attributes like @Position, @Direction to support the implementation 
 */
public final class RobotActionImpl extends BaseRobotActions {

	
	@Override
	public Position move(Robot robot) throws RobotApplicationException{
		Position newPosition = stepForward(robot.getPosition());
		if (isDangerZone(newPosition)) {
			//System.out.println("DANGER moving to " + newPosition.toString());
			throw new RobotApplicationException(ErrCodeEnum.ERR_OUT_OF_BOUNDARY);
		}
		return newPosition;
	}

	@Override
	public Position turnRight(Robot robot) {

		robot.setPosition(turnRight(robot.getPosition()));
		return robot.getPosition();
	}
	@Override
	public Position turnLeft(Robot robot) {

		robot.setPosition(turnLeft(robot.getPosition()));
		return robot.getPosition();
	}

	public void validateMovement(Robot robot,Position newPosition) {
		if (isDangerZone(newPosition)) {
			System.out.println("DANGER moving to " + newPosition.toString());
		} else {
			System.out.println("SUCCESSFULL moving to "
					+ newPosition.toString());
			robot.setPosition(newPosition);
		}
	}

	/**
	 * 
	 * @Purpose This method is created for a 2D SQAURE plane movement of a
	 *          robot.
	 * *//*
	public boolean isDangerZoneOld(Position newPosition) {
		// CORNER 00
		if ((newPosition.getxAxis() < RobotConstants.MIN_XAXIS & newPosition.getyAxis() < RobotConstants.MIN_YAXIS)
				&& (newPosition.getFace() == Direction.WEST || newPosition.getFace() == Direction.SOUTH)) {
			return true;
		}
		// CORNER 55
		else if ((newPosition.getxAxis() > RobotConstants.MAX_XAXIS  & newPosition.getyAxis() > RobotConstants.MAX_YAXIS )
				&& (newPosition.getFace() == Direction.EAST || newPosition.getFace() == Direction.NORTH)) {
			return true;
		}
		// CORNER 05
		else if ((newPosition.getxAxis() >= RobotConstants.MIN_XAXIS & newPosition.getyAxis() > RobotConstants.MAX_YAXIS)
				&& (newPosition.getFace() == Direction.WEST || newPosition.getFace() == Direction.NORTH)) {
			return true;
		}
		// CORNER 50
		else if ((newPosition.getxAxis() > RobotConstants.MAX_XAXIS & newPosition.getyAxis() >= RobotConstants.MIN_YAXIS)
				&& (newPosition.getFace() == Direction.EAST || newPosition.getFace() == Direction.SOUTH)) {
			return true;
		}
		if (newPosition.getyAxis() < RobotConstants.MIN_YAXIS && (newPosition.getFace() == Direction.SOUTH)) {
			return true;
		}
		if (newPosition.getxAxis() < RobotConstants.MIN_XAXIS && (newPosition.getFace() == Direction.WEST)) {
			return true;
		}
		return false;
	}
	 */
	/**
	 * 
	 * @Purpose This method is created for a 2D SQAURE plane movement of a
	 *          robot.
	 * **/
	public boolean isDangerZone(Position newPosition) {
		
		if ((newPosition.getyAxis() < RobotConstants.MIN_YAXIS || newPosition
				.getyAxis() > RobotConstants.MAX_YAXIS)
				|| (newPosition.getxAxis() < RobotConstants.MIN_XAXIS || newPosition
						.getxAxis() > RobotConstants.MAX_XAXIS)) {
			return true;
		}
		
		return false;
	}
}
