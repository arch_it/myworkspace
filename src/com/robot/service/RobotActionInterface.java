/**
 * 
 */
package com.robot.service;

import com.robot.service.model.Position;
import com.robot.service.model.Robot;
import com.robot.services.exception.RobotApplicationException;

/**
 * @author ARCHANA RAJASEKARAN
 *
 */
public interface RobotActionInterface {
	
	public Position turnRight(Robot robot);
	public Position turnLeft(Robot robot);
	public Position move(Robot robot) throws RobotApplicationException;
	public void validateMovement(Robot robot,Position newPosition);
	public boolean isDangerZone(Position newPosition);

}
