package com.robot.service.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Position {
    Direction face;
	Integer xAxis;
	public Direction getFace() {
		return face;
	}
	public void setFace(Direction face) {
		this.face = face;
	}
	public Integer getxAxis() {
		return xAxis;
	}
	public void setxAxis(Integer xAxis) {
		this.xAxis = xAxis;
	}
	public Integer getyAxis() {
		return yAxis;
	}
	public void setyAxis(Integer yAxis) {
		this.yAxis = yAxis;
	}
	Integer yAxis;
	
	public Position(){
		this.face= Direction.NORTH;
		this.xAxis =0;
		this.yAxis=0;
		
	}
	@Override
	public String toString() {
		return "Position [face=" + face + ", xAxis=" + xAxis + ", yAxis="
				+ yAxis + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if( obj instanceof Position  ){
			Position newPos = (Position)obj;
			if(newPos.xAxis == this.xAxis && newPos.yAxis == this.yAxis && newPos.face == this.face){
				return true;
			} 
		}
		return false;
	}
	public Position(Integer xAxis,Integer yAxis,Direction face){
		this.face = face;
		this.xAxis = xAxis;
		this.yAxis = yAxis;
	}
}
