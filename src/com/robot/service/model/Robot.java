package com.robot.service.model;
//import javax.xml.bind.annotation.XmlRootElement;

import java.util.Map;
import java.util.TreeMap;

import com.robot.constants.ActionConstant;
import com.robot.services.exception.ErrCodeEnum;
import com.robot.services.exception.RobotApplicationException;


public class Robot {

	String name;
	Position position;
	Map<String,Position> positionTracker;
	int i=0;

	public Robot(String name) {
		super();
		this.name = name;
	}
	public Robot(String name, Position position) {
		super();
		this.name = name;
		this.position = position;
		
		/**
		 * This is a costly tracking object. Temporarly included to track instead of
		 * loggers
		 **/
		this.positionTracker = new TreeMap<String,Position>(); 
		if(position.getxAxis()>5)
			this.position.setxAxis(5);
		if(position.getyAxis()>5)
			this.position.setyAxis(5);
		if(position.getxAxis()<0)
			this.position.setxAxis(0);
		if(position.getyAxis()<0)
			this.position.setyAxis(0);
	}
	
	
	public Map<String, Position> getPositionTracker() {
		return positionTracker;
	}

	/**
	 * This is a costly tracking object. Temporarly included to track instead of
	 * loggers
	 **/
	public void setPositionTracker(ActionConstant mov, Position position) {
		String generator = ++i +"->" + mov.name();
		positionTracker.put(generator, position);
	}
	public void validatePosition(Position position) throws RobotApplicationException{
		
		if(position.getxAxis()>5 || position.getyAxis()>5 || position.getxAxis()<0 || position.getyAxis()<0 )
			throw new RobotApplicationException(ErrCodeEnum.ERR_OUT_OF_BOUNDARY);
		

		
	}
	@Override
	public String toString() {
		return "Name ["+name+"] Position ["+position.toString()+"]";
	}
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public void setPosition(Position position) {
		
		this.position = position;
	}
	

	public Position getPosition(){
	return position;	
		
	}	
	

}
